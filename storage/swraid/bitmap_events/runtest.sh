#!/bin/bash
#
# Copyright (c) 2024 Red Hat, Inc. All rights reserved.
#
# This copyrighted material is made available to anyone wishing
# to use, modify, copy, or redistribute it subject to the terms
# and conditions of the GNU General Public License version 2.
#
# This program is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, USA.
#


FILE=$(readlink -f ${BASH_SOURCE[0]})
CDIR=$(dirname $FILE)

source  "$CDIR"/../../../cki_lib/libcki.sh || exit 1

# TEST is required for beakerlib tests
export TEST=${RSTRNT_TASKNAME}


function runtest()
{

	# Create a MD RAID level 5
	rlRun "/usr/sbin/mdadm -CR /dev/md0 -l5 -n5 /dev/loop[0-4] --force --bitmap=internal"

	if [ $? -ne 0 ];then
	rlFail "FAIL: Failed to create md raid $RETURN_STR"
	return
	fi

	rlLog "INFO: Successfully created md raid $RETURN_STR"

	# Make a filesystem on the MD RAID array
	mkfs.ext4 -F /dev/md0
	mkdir -p /mnt/md_test
	mount /dev/md0 /mnt/md_test
	rlRun "/usr/bin/dd if=/dev/urandom of=/mnt/md_test/testfile bs=1M count=100"
	rlLog "MD test array is built"

	rlLog "calculate a checksum on the testfile"
	rlRun "/usr/bin/md5sum /mnt/md_test/testfile > /mnt/md_test/checksumbefore"

	rlLog "Show the Events for each disk's bitmap"
	rlRun "/usr/sbin/mdadm --examine-bitmap /dev/loop[0-4] | grep Events"

	rlLog "Check MD array state"
	rlLog array_state = "$(</sys/block/md0/md/array_state )"
	rlLog sync_action = "$(</sys/block/md0/md/sync_action )"

	rlLog "waiting for initial MD array to sync"
	rlRun "/usr/sbin/mdadm --wait /dev/md0"

	rlLog "Fail a device from the array"
	rlRun "/usr/sbin/mdadm -f /dev/md0 /dev/loop4"
	rlLog "Remove a device from the array"
	rlRun "/usr/sbin/mdadm -r /dev/md0 /dev/loop4"

	rlLog "Check MD array state"
	rlLog array_state = "$(</sys/block/md0/md/array_state )"
	rlLog sync_action = "$(</sys/block/md0/md/sync_action )"

	rlLog "Wait for MD array to sync"
#	rlRun "/usr/sbin/mdadm --wait /dev/md0"

	rlRun "/usr/sbin/mdadm --examine-bitmap /dev/loop[0-4] | grep Events"
	rlLog "Add the device into the array"
	rlRun "/usr/sbin/mdadm --add /dev/md0 /dev/loop4"
	rlRun "/usr/sbin/mdadm --examine-bitmap /dev/loop[0-4] | grep Events"

	rlLog "Check MD array state"
	rlLog array_state = "$(</sys/block/md0/md/array_state )"
	rlLog sync_action = "$(</sys/block/md0/md/sync_action )"
	rlLog "Wait 5 seconds"
	sleep 5
	rlLog sync_action = "$(</sys/block/md0/md/sync_action )"

	rlLog "Show the Events for each disk's bitmap"
	rlRun "/usr/sbin/mdadm --examine-bitmap /dev/loop[0-4] | grep Events"

	rlLogo "Check the checksum"
	rlRun "/usr/bin/md5sum /mnt/md_test/testfile > /mnt/md_test/checksumafter"

	rlLog "See if there is any difference in the checksum"
	rlRun "/usr/bin/diff /mnt/md_test/checksumbefore /mnt/md_test/checksumafter > /mnt/md_test/checksumsize"
	if [ -s /mnt/md_test/checksumsize ]
	then
		rlFail "Checksum Failed"
	else
		rlPass "Checksum Passed"
	fi

	# Run cleanup - to make sure environment is ready
	cleanup


}

function startup
{
	for i in {0..4};do
	rlRun "dd if=/dev/urandom of=/opt/loop_$i bs=1M count=500"
	done

	for i in {0..4};do
	rlRun "/usr/sbin/losetup /dev/loop$i /opt/loop_$i"
	done

	return $CKI_PASS
}


function cleanup
{
	rlLog "Cleaning up MD array"
	rlRun "/usr/bin/rm -Rf /mnt/md_test/testfile"
	rlRun "/usr/bin/rm -Rf /mnt/md_test/checksum*"
	umount /mnt/md_test
	rlRun "/usr/sbin/mdadm -Ss"
	rlRun "/usr/sbin/losetup -D /dev/loop*"
}



cki_main
exit $?
